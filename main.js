const SLASH_KEY = "/";
const ENTER_KEY = "Enter";
const BACKSPACE_KEY = "Backspace";

onPageLoad();

/**
 * DATA STATE
 */
// focus block element reference
let focusBlockElRef;
// state for current added tag
let currentAddedTag;

function onPageLoad() {
  new FroalaEditor(".froala-editor", {
    toolbarContainer: "#toolbar-container",
    toolbarButtons: [
      "fullscreen",
      "bold",
      "italic",
      "underline",
      "strikeThrough",
      "subscript",
      "superscript",
      "|",
      "fontFamily",
      "fontSize",
      "color",
      "inlineStyle",
      "paragraphStyle",
      "|",
      "paragraphFormat",
      "align",
      "formatOL",
      "formatUL",
      "outdent",
      "indent",
      "quote",
      "-",
      "insertLink",
      "insertImage",
      "insertVideo",
      "insertFile",
      "insertTable",
      "|",
      "emoticons",
      "specialCharacters",
      "insertHR",
      "selectAll",
      "clearFormatting",
      "|",
      "print",
      "help",
      "html",
      "|",
      "undo",
      "redo",
      "trackChanges",
      "markdown",
    ],
  });
  document.addEventListener("keydown", (e) => {
    // While pressing "/" should show modal popup under the block
    if (e.key === SLASH_KEY) {
      showModal();
    }
    // While pressing "Enter" should add a new block to the page
    if (e.key === ENTER_KEY) {
      e.preventDefault();
      addNewBlock();
      hideModal();
    }
    // While pressing "Backspace" should remove element from focus block
    if (e.key === BACKSPACE_KEY) {
      removeElementFromFocusBlock(e);
    }
    onChecboxClick();
  });
  // remove modal when other palce is clicked
  document.addEventListener("click", (e) => {
    if (e.target.id !== "modal") {
      hideModal();
    }
  });
  onSelectItem();
  hideVideoPopup();
  // onBackDropTableControlClick();
  addFroalaEditor();

  agGridConfig();
}

function addFroalaEditor() {
  const froalaEditors = document.querySelectorAll(".add-froala__btn");
  for (const [index, editor] of Object.entries(froalaEditors)) {
    editor.addEventListener("click", (e) => {
      const wrapper = e.target.parentElement;
      const block = wrapper.querySelector(`.froala-editor--${+index + 1}`);
      if (editor.innerHTML === "−") {
        editor.innerHTML = "&#43;";
      } else {
        editor.innerHTML = "&#8722;";
      }
      block.classList.toggle("show");
    });
  }
}

function hideModal() {
  const modalEl = document.querySelector(".modal");
  modalEl.classList.remove("show");
}

function showModal() {
  const focusBlockEl = document.querySelector(".block:focus");
  // avoid opening modal when block is not empty
  if (focusBlockEl.innerText || focusBlockEl.innerHTML) return;

  focusBlockElRef = focusBlockEl;

  const modalEl = document.querySelector(".modal");
  modalEl.classList.add("show");

  const { x, y } = getCaretCoordinates();
  modalEl.style.top = `${y - 20}px`;
  modalEl.style.left = `${x}px`;
}

function removeElementFromFocusBlock(e) {
  const wrapperEl = document.querySelector(".content-wrapper");
  const focusBlockEl = wrapperEl.querySelector(".block:focus");

  // avoid first block to be removed
  const firstBlockEl = wrapperEl.querySelector(".block");
  // set placeholder in the first block when the block only one element
  if (wrapperEl.querySelectorAll(".block").length === 1) {
    firstBlockEl.setAttribute("data-text", "Enter text here");
  }

  if (focusBlockEl === firstBlockEl) return;

  if (focusBlockEl.tagName === "LI" && focusBlockEl.innerText === "") {
    const parentEl = focusBlockEl.parentElement;
    const listEl = parentEl.querySelectorAll(".block");
    if (listEl.length === 1 && listEl[0].innerText === "") {
      parentEl.remove();
      const newBlock = document.createElement("div");
      newBlock.classList.add("block");
      newBlock.setAttribute("contenteditable", true);
      wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
      newBlock.focus();
      return;
    }
  }

  if (focusBlockEl.tagName === "SPAN" && focusBlockEl.innerText === "") {
    const parentEl = focusBlockEl.parentElement;
    const newBlock = document.createElement("div");
    newBlock.classList.add("block");
    newBlock.setAttribute("contenteditable", true);
    wrapperEl.insertBefore(newBlock, parentEl.nextElementSibling);
    newBlock.focus();
    parentEl.remove();
    return;
  }

  let previousBlockEl = focusBlockEl.previousElementSibling;
  if (previousBlockEl.tagName === "UL") {
    previousBlockEl = previousBlockEl.lastElementChild;
  } else if (previousBlockEl.classList.contains("checkbox-layout")) {
    previousBlockEl = previousBlockEl.querySelector(".block-checkbox-span");
  }

  if (focusBlockEl.innerText === "") {
    e.preventDefault();
    // remove focus block
    if (focusBlockEl.tagName === "LI") {
      focusBlockEl.parentElement.removeChild(focusBlockEl);
      const newBlock = document.createElement("div");
      newBlock.classList.add("block");
      newBlock.setAttribute("contenteditable", true);
      wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
      previousBlockEl = newBlock;
    } else if (previousBlockEl.classList.contains("block-table")) {
      const lastTableCell =
        previousBlockEl.querySelectorAll(".block-table-cell").length - 1;
      wrapperEl.removeChild(focusBlockEl);
      setCaretToEnd(
        previousBlockEl.querySelectorAll(".block-table-cell")[lastTableCell]
      );
      return;
    } else {
      wrapperEl.removeChild(focusBlockEl);
    }
    // change focus to previous block the end of the character
    setCaretToEnd(previousBlockEl);
    // change focus to previous block
    previousBlockEl.focus();
  }
}

function addNewBlock() {
  const wrapperEl = document.querySelector(".content-wrapper");
  let newBlock = document.createElement("div");
  newBlock.classList.add("block");

  // add new attribute to new block
  newBlock.setAttribute("contenteditable", true);
  // append new block to wrapper after focus block inside ul tag
  let focusBlockEl = wrapperEl.querySelector(".block:focus");
  if (focusBlockEl.tagName === "LI" && focusBlockEl.innerText !== "") {
    const focusBlockParentEl = focusBlockEl.parentElement;
    newBlock = document.createElement("li");
    newBlock.classList.add("block");
    newBlock.classList.add("block-li");
    newBlock.setAttribute("contenteditable", true);
    focusBlockParentEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
    // change focus to new block
    newBlock.focus();
  } else if (focusBlockEl.tagName === "LI" && focusBlockEl.innerText === "") {
    // if li tag is empty remove it
    // swap li with div and close parent ul tag
    focusBlockEl = focusBlockEl.parentElement;
    focusBlockEl.removeChild(focusBlockEl.lastChild);
    newBlock = document.createElement("div");
    newBlock.classList.add("block");
    newBlock.setAttribute("contenteditable", true);
    wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
    // change focus to new block
    newBlock.focus();
  } else if (focusBlockEl.tagName === "SPAN" && focusBlockEl.innerText !== "") {
    // create a new checkbox layout and append it to the sibling of focus block parent
    newBlock.removeAttribute("contenteditable");
    newBlock.classList.add("checkbox-layout");
    const childCheckbox = document.createElement("input");
    childCheckbox.setAttribute("type", "checkbox");
    childCheckbox.classList.add("block-checkbox");
    newBlock.appendChild(childCheckbox);
    const childSpan = document.createElement("span");
    childSpan.classList.add("block");
    childSpan.classList.add("block-checkbox-span");
    childSpan.setAttribute("contenteditable", true);
    newBlock.appendChild(childSpan);
    focusBlockEl = focusBlockEl.parentElement;
    wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
    newBlock.querySelector(".block-checkbox-span").focus();
  } else if (focusBlockEl.tagName === "SPAN" && focusBlockEl.innerText === "") {
    // if span tag is empty remove it
    // parent tag with editable div tag
    focusBlockEl = focusBlockEl.parentElement;
    newBlock = document.createElement("div");
    newBlock.classList.add("block");
    newBlock.setAttribute("contenteditable", true);
    wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
    // change focus to new block
    newBlock.focus();
    focusBlockEl.remove();
  } else {
    wrapperEl.insertBefore(newBlock, focusBlockEl.nextElementSibling);
    // change focus to new block
    newBlock.focus();
  }

  // remove data-text attribute placeholder from first block
  const firstBlockEl = wrapperEl.querySelector(".block");
  firstBlockEl.removeAttribute("data-text");
}

function setCaretToEnd(element) {
  // Create a new range
  const range = document.createRange();
  // Get the selection object
  const selection = window.getSelection();
  // Select all the content from the contenteditable element
  range.selectNodeContents(element);
  // Collapse it to the end, i.e. putting the cursor to the end
  range.collapse(false);
  // Clear all existing selections
  selection.removeAllRanges();
  // Put the new range in place
  selection.addRange(range);
  // Set the focus to the contenteditable element
  element.focus();
}

function getCaretCoordinates() {
  let x, y;
  const rect = focusBlockElRef.getBoundingClientRect();
  x = rect.left;
  y = rect.top;
  return { x, y };
}

function onSelectItem() {
  // add event listener to image upload button
  const imageUploadBtn = document.querySelector(".select-item--image");
  // imageUploadBtn.addEventListener("change", (e) => {
  //   const newBlock = document.createElement("div");
  //   newBlock.classList.add("block");
  //   newBlock.classList.add("block-image");
  //   const btn = document.createElement("button");
  //   btn.classList.add("block-image-btn");
  //   btn.innerHTML = "&#10006";
  //   // btn.innerHTML = "&#128247";&#10006;
  //   const image = document.createElement("img");
  //   image.src = URL.createObjectURL(e.target.files[0]);
  //   const wrapper = document.querySelector(".content-wrapper");
  //   newBlock.appendChild(btn);
  //   newBlock.appendChild(image);
  //   wrapper.insertBefore(newBlock, focusBlockElRef.nextElementSibling);
  //   focusBlockElRef.remove();
  //   const newDiv = document.createElement("div");
  //   newDiv.classList.add("block");
  //   newDiv.setAttribute("contenteditable", true);
  //   wrapper.insertBefore(newDiv, newBlock.nextElementSibling);
  //   onRemoveImage();
  // });
  // select-item
  const items = document.querySelectorAll(".select-item");
  for (const item of items) {
    // add event listener on select item click
    item.addEventListener("click", (e) => {
      // create new element base on selected item
      const tag = e.target.getAttribute("data-tag");

      if (tag === "iframe") {
        const videoPopup = document.querySelector(".video-link-pop-up");
        videoPopup.classList.add("show");
        onCreateVideoLink();
        return;
      }

      let newBlockEl = document.createElement(tag);
      if (tag === "checbox") {
        newBlockEl = document.createElement("div");
        newBlockEl.classList.add("checkbox-layout");
        const childCheckbox = document.createElement("input");
        childCheckbox.setAttribute("type", "checkbox");
        childCheckbox.classList.add("block-checkbox");
        newBlockEl.appendChild(childCheckbox);
        const childSpan = document.createElement("span");
        childSpan.classList.add("block");
        childSpan.classList.add("block-checkbox-span");
        childSpan.setAttribute("contenteditable", true);
        newBlockEl.appendChild(childSpan);
      }
      // if tag is ul add child li
      if (tag === "ul") {
        const child = document.createElement("li");
        child.setAttribute("contenteditable", true);
        child.classList.add("block");
        child.classList.add("block-li");
        newBlockEl.appendChild(child);
      }

      // if tag is table create table with 3 rows and 3 columns
      if (tag === "table") {
        const tableWrapper = document.createElement("div");
        tableWrapper.classList.add("block");
        tableWrapper.classList.add("block-table-wrapper");

        const headerWrapper = document.createElement("div");
        headerWrapper.classList.add("header-wrapper");

        const title = document.createElement("h2");
        title.classList.add("no-margin");
        title.classList.add("table-title");
        title.setAttribute("contenteditable", true);
        title.innerHTML = "Title";

        const navWrapper = document.createElement("nav");
        navWrapper.classList.add("nav-wrapper");

        const btnFilter = document.createElement("button");
        btnFilter.classList.add("btn-filter");
        btnFilter.innerHTML = "Filter";

        const btnSort = document.createElement("button");
        btnSort.classList.add("btn-sort");
        btnSort.setAttribute("id", "btn-sort");
        btnSort.innerHTML = "Sort";

        navWrapper.appendChild(btnFilter);
        navWrapper.appendChild(btnSort);

        headerWrapper.appendChild(title);
        headerWrapper.appendChild(navWrapper);
        tableWrapper.appendChild(headerWrapper);

        const table = document.createElement("table");
        table.classList.add("block-table");

        const addRowTableBtn = document.createElement("button");
        addRowTableBtn.classList.add("block-table-add-row-btn");
        addRowTableBtn.innerHTML = "+";
        table.appendChild(addRowTableBtn);

        const addColumnTableBtn = document.createElement("button");
        addColumnTableBtn.classList.add("block-table-add-column-btn");
        addColumnTableBtn.innerHTML = "+";
        table.appendChild(addColumnTableBtn);

        const removeLastRow = document.createElement("button");
        removeLastRow.classList.add("block-table-remove-row-btn");
        removeLastRow.innerHTML = "-";
        table.appendChild(removeLastRow);

        const removeLastColumn = document.createElement("button");
        removeLastColumn.classList.add("block-table-remove-column-btn");
        removeLastColumn.innerHTML = "-";
        table.appendChild(removeLastColumn);

        const removeTable = document.createElement("button");
        removeTable.classList.add("block-table-remove-table-btn");
        removeTable.innerHTML = "x";
        table.appendChild(removeTable);

        tableWrapper.appendChild(table);

        for (let i = 0; i < 3; i++) {
          const row = document.createElement("tr");
          for (let j = 0; j < 3; j++) {
            const cell =
              i === 0
                ? document.createElement("th")
                : document.createElement("td");
            cell.setAttribute("contenteditable", true);
            cell.classList.add("block");
            cell.classList.add("block-table-cell");
            row.appendChild(cell);
          }
          table.appendChild(row);
        }
        newBlockEl = tableWrapper;
      }

      newBlockEl.classList.add("block");
      // avoid putting attribute contenteditable to ul
      if (tag !== "ul" && tag !== "checbox" && tag !== "table") {
        newBlockEl.setAttribute("contenteditable", true);
      }

      // swap div tag with selected item tag
      focusBlockElRef.parentNode.replaceChild(newBlockEl, focusBlockElRef);
      // Adding focus to new block
      // if tag is ul put caret to li
      let caretPosition = newBlockEl;
      if (tag === "ul") {
        caretPosition = newBlockEl.firstChild;
      } else if (tag === "checbox") {
        caretPosition = newBlockEl.querySelector(".block-checkbox-span");
      } else if (tag === "table") {
        caretPosition = newBlockEl.querySelector(".block-table-cell");
      }
      setCaretToEnd(caretPosition);
      onAddNewRow();
      onAddNewColum();
      onRemoveLastRow();
      onRemoveLastColumn();
      onRemoveTable();
      buttonFilterHandler();
      buttonSortHandler();
    });
  }
}

function onChecboxClick() {
  // checkbox click
  const checkboxEl = document.querySelectorAll(".block-checkbox");
  for (const checkbox of checkboxEl) {
    checkbox.addEventListener("click", (e) => {
      const checkboxSpan = e.target.parentElement.querySelector(
        ".block-checkbox-span"
      );
      if (checkbox.checked) {
        checkboxSpan.classList.add("checked");
      } else {
        checkboxSpan.classList.remove("checked");
      }
    });
  }
}

function onAddNewRow() {
  const btns = document.querySelectorAll(".block-table-add-row-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const table = e.target.parentElement;
      const row = document.createElement("tr");
      const columnsLength = table.querySelectorAll("th").length;
      for (let i = 0; i < columnsLength; i++) {
        const cell = document.createElement("td");
        cell.setAttribute("contenteditable", true);
        cell.classList.add("block");
        cell.classList.add("block-table-cell");
        row.appendChild(cell);
      }
      table.appendChild(row);
    });
  }
}

function onAddNewColum() {
  const btns = document.querySelectorAll(".block-table-add-column-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const table = e.target.parentElement;
      const rowsLength = table.querySelectorAll("tr").length;
      for (let i = 0; i < rowsLength; i++) {
        const row = table.querySelectorAll("tr")[i];
        const cell =
          i === 0 ? document.createElement("th") : document.createElement("td");
        cell.setAttribute("contenteditable", true);
        cell.classList.add("block");
        cell.classList.add("block-table-cell");
        row.appendChild(cell);
      }
    });
  }
}

function onRemoveLastRow() {
  const btns = document.querySelectorAll(".block-table-remove-row-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const table = e.target.parentElement;
      const rowsLength = table.querySelectorAll("tr").length;
      if (rowsLength > 1) {
        table.removeChild(table.lastChild);
      }
    });
  }
}

function onRemoveLastColumn() {
  const btns = document.querySelectorAll(".block-table-remove-column-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const table = e.target.parentElement;
      table.querySelectorAll("tr").forEach((row) => {
        if (
          row.querySelectorAll("td").length > 1 ||
          row.querySelectorAll("th").length > 1
        ) {
          row.removeChild(row.lastChild);
        }
      });
    });
  }
}

function onRemoveTable() {
  const btns = document.querySelectorAll(".block-table-remove-table-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const table = e.target.parentElement;
      const div = document.createElement("div");
      div.classList.add("block");
      div.setAttribute("contenteditable", true);
      table.parentNode.replaceChild(div, table);
      setCaretToEnd(div);
    });
  }
}

function onRemoveImage() {
  const btns = document.querySelectorAll(".block-image-btn");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const image = e.target.parentElement;
      const div = document.createElement("div");
      div.classList.add("block");
      div.setAttribute("contenteditable", true);
      const wrapper = document.querySelector(".content-wrapper");
      wrapper.insertBefore(div, image);
      image.remove();
      setCaretToEnd(div);
    });
  }
}

// Function to hide video popup
function hideVideoPopup() {
  const videoPopupBackdrop = document.querySelector(".video-link-backdrop");
  videoPopupBackdrop.addEventListener("click", () => {
    videoPopupBackdrop.parentElement.classList.remove("show");
  });
}

function onCreateVideoLink() {
  const btnSubmit = document.querySelector(".video-link-create");
  btnSubmit.addEventListener("click", btnCreateVideoHandler);
}

function onRemoveVideo() {
  const btns = document.querySelectorAll(".block-video-close");
  for (const btn of btns) {
    btn.addEventListener("click", (e) => {
      const video = e.target.parentElement;
      const div = document.createElement("div");
      div.classList.add("block");
      div.setAttribute("contenteditable", true);
      const wrapper = document.querySelector(".content-wrapper");
      wrapper.insertBefore(div, video);
      video.remove();
      setCaretToEnd(div);
    });
  }
}

function btnCreateVideoHandler(e) {
  e.preventDefault();
  const videoLinkInput = document.querySelector(".video-link-input");
  const videoLink = videoLinkInput.value;
  // const videoLinkRegex =
  //   /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/g;
  // if (!videoLink.match(videoLinkRegex)) {
  //   videoLinkInput.classList.add("error");
  //   return;
  // }
  const videoId = videoLink.split("=")[1];
  const embededLink = `https://www.youtube.com/embed/${videoId}`;
  const wrapper = document.querySelector(".content-wrapper");

  const iframeWrapper = document.createElement("div");
  iframeWrapper.classList.add("block");
  iframeWrapper.classList.add("block-video");

  const buttonClose = document.createElement("button");
  buttonClose.classList.add("block-video-close");
  buttonClose.innerHTML = "&times;";

  const iframe = document.createElement("iframe");
  iframe.setAttribute("src", embededLink);
  iframe.classList.add("block");
  iframe.setAttribute("width", "500");
  iframe.setAttribute("height", "283");

  iframeWrapper.appendChild(buttonClose);
  iframeWrapper.appendChild(iframe);

  wrapper.insertBefore(iframeWrapper, focusBlockElRef.nextElementSibling);
  focusBlockElRef.remove();

  const div = document.createElement("div");
  div.classList.add("block");
  div.setAttribute("contenteditable", true);
  wrapper.insertBefore(div, iframe.nextElementSibling);

  setCaretToEnd(div);
  const videoPopup = document.querySelector(".video-link-pop-up");
  videoPopup.classList.remove("show");
  videoLinkInput.value = "";
  const form = document.querySelector(".video-link-form");
  form.reset();
  onRemoveVideo();
}

function buttonFilterHandler() {
  const btnFilters = document.querySelectorAll(".btn-filter");
  for (const btn of btnFilters) {
    btn.addEventListener("click", (e) => {
      // const filterPopup = document.querySelector(".dropdown-type-content");
      // filterPopup.classList.toggle("show");
    });
  }
}

function buttonSortHandler() {
  const btnSorts = document.querySelectorAll(".btn-sort");
  for (const btn of btnSorts) {
    btn.addEventListener("click", (e) => {
      const data = btn.parentElement.parentElement.parentElement;
      colsHandler(data);
      onSelectColHandler(data);
      const sortPopup = document.querySelector(".table-control-wrapper");
      sortPopup.classList.toggle("show");
    });
  }
  sortColumnAndTypeHandler();
}

function colsHandler(data) {
  console.log(data);
  const colTitles = data.querySelectorAll("th");
  const dropdownColContent = document.querySelector(".dropdown-col-content");
  for (const col of colTitles) {
    const div = document.createElement("div");
    div.classList.add("dropdown-col-item");
    div.innerHTML = !col.innerHTML ? "[None]" : col.innerHTML;
    dropdownColContent.appendChild(div);
  }
}

function sortColumnAndTypeHandler() {
  const btnSortCol = document.querySelector(".dropdown-col-btn");
  btnSortCol.addEventListener("click", (e) => {
    const dropdownCol = document.querySelector(".dropdown-col-content");
    dropdownCol.classList.toggle("show");
  });

  const btnType = document.querySelector(".dropdown-type-btn");
  btnType.addEventListener("click", (e) => {
    const dropdownType = document.querySelector(".dropdown-type-content");
    dropdownType.classList.toggle("show");
  });
}

function onBackDropTableControlClick() {
  const backdrop = document.querySelector(".table-control-backdrop");
  backdrop.addEventListener("click", (e) => {
    const tableControl = document.querySelector(".table-control-wrapper");
    tableControl.classList.remove("show");
    const dropdownColContent = document.querySelector(".dropdown-col-content");
    dropdownColContent.innerHTML = "";

    const dropdownCol = document.querySelector(".dropdown-col-content");
    dropdownCol.classList.remove("show");
    const dropdownType = document.querySelector(".dropdown-type-content");
    dropdownType.classList.remove("show");
  });
}

function onSelectColHandler(data) {
  const items = document.querySelectorAll(".dropdown-col-item");
  for (const [key, item] of Object.entries(items)) {
    item.addEventListener("click", (e) => {
      console.log(data, key, item);
    });
  }
}

// AG GRID CONFIG
function sortByAthleteAsc() {
  gridOptions.columnApi.applyColumnState({
    state: [{ colId: "athlete", sort: "asc" }],
    defaultState: { sort: null },
  });
}

function sortByAthleteDesc() {
  gridOptions.columnApi.applyColumnState({
    state: [{ colId: "athlete", sort: "desc" }],
    defaultState: { sort: null },
  });
}

function sortByCountryThenSport() {
  gridOptions.columnApi.applyColumnState({
    state: [
      { colId: "country", sort: "asc", sortIndex: 0 },
      { colId: "sport", sort: "asc", sortIndex: 1 },
    ],
    defaultState: { sort: null },
  });
}

function sortBySportThenCountry() {
  gridOptions.columnApi.applyColumnState({
    state: [
      { colId: "country", sort: "asc", sortIndex: 1 },
      { colId: "sport", sort: "asc", sortIndex: 0 },
    ],
    defaultState: { sort: null },
  });
}

function clearSort() {
  gridOptions.columnApi.applyColumnState({
    defaultState: { sort: null },
  });
}

var savedSort;

function saveSort() {
  var colState = gridOptions.columnApi.getColumnState();
  var sortState = colState
    .filter(function (s) {
      return s.sort != null;
    })
    .map(function (s) {
      return { colId: s.colId, sort: s.sort, sortIndex: s.sortIndex };
    });
  savedSort = sortState;
  console.log("saved sort", sortState);
}

function restoreFromSave() {
  gridOptions.columnApi.applyColumnState({
    state: savedSort,
    defaultState: { sort: null },
  });
}

function agGridConfig() {
  const columnDefs = [
    { field: "athlete" },
    { field: "age", width: 90, filter: "agNumberColumnFilter" },
    { field: "country" },
    { field: "year", width: 90 },
    { field: "date", filter: "agDateColumnFilter" },
    { field: "sport" },
    { field: "gold", filter: "agNumberColumnFilter" },
    { field: "silver", filter: "agNumberColumnFilter" },
    { field: "bronze", filter: "agNumberColumnFilter" },
    { field: "total", filter: "agNumberColumnFilter" },
  ];

  // let the grid know which columns to use
  const gridOptions = {
    columnDefs: columnDefs,
    defaultColDef: {
      sortable: true,
      filter: true,
      editable: true,
    },
  };

  // lookup the container we want the Grid to use
  const eGridDiv = document.querySelector("#myGrid");

  const columnDefs2 = [
    { field: "make" },
    { field: "model" },
    { field: "price" },
  ];

  // specify the data
  const rowData2 = [
    { make: "Toyota", model: "Celica", price: 35000 },
    { make: "Ford", model: "Mondeo", price: 32000 },
    { make: "Porsche", model: "Boxter", price: 72000 },
  ];

  // let the grid know which columns and what data to use
  const gridOptions2 = {
    columnDefs: columnDefs2,
    rowData: rowData2,
    defaultColDef: {
      sortable: true,
      filter: true,
      editable: true,
    },
  };

  const eGridDivSecond = document.querySelector("#myGrid-2");

  // create the grid passing in the div to use together with the columns & data we want to use
  new agGrid.Grid(eGridDiv, gridOptions);
  new agGrid.Grid(eGridDivSecond, gridOptions2);

  // fetch the row data to use and one ready provide it to the Grid via the Grid API
  fetch("https://www.ag-grid.com/example-assets/olympic-winners.json")
    .then((response) => response.json())
    .then((data) => gridOptions.api.setRowData(data));
}
